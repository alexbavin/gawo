from math import*
from argparse import*
from re import T 
import sys 

name = ""
out = ""
text = ""

tokens = {"int":"int",
          "float":"float",
          "string":"string",
          "bool":"bool",
          "+":"add",
          "-":"sub",
          "*":"mul",
          "/":"div",
          "break":"break",
          "exit":"exit",
          "print":"print",
          "inc":"inc",
          "dec":"dec",
          "++":"inc",
          "--":"dec",
          "getline":"getline",
          #"if":"ifPrim",
          "while":"whileprim",
          "=":"set",
          "==":"eq",
          "!!":"neq",
          ">":"gr",
          "<":"sm",
          "<=":"sme",
          ">=":"gre",
          "{":"clparen",
          "}":"crparen",
          "(":"lparen",
          ")":"rparen"
          }#evtl // % and or not 

class lexer:
    def __init__(self):
        self.tokens = [] 
    def lex(self,text):
        #remove new lines
        result = [] 
        text = text.replace("\n","")
        text = text.replace ("\t","") 
        text = text.replace("("," ( ")
        text = text.replace(")"," ) ")
        text = text.replace("{"," { ")
        text = text.replace("++"," ++")
        text = text.replace("--"," --")
        text = text.replace("+"," + ")  
        text = text.replace("-"," - ")  
        text = text.replace("*"," * ")  
        text = text.replace("/"," / ")  
        #text = text.replace("="," = ")  
        #text = text.replace(">"," > ")  
        #text = text.replace("<"," < ")  
        text = text.replace("=="," == ")  
        text = text.replace(">="," >= ")  
        text = text.replace("<="," <= ")  
        text = text.replace("!!"," !! ")  
 
  
        lines = text.split(";")
        for a in lines:
            line = a.split(" ")
            result.append(line)
        for a in range(0,len(result),1): 
            for a1 in range(0,len(result[a]),1):
                try: 
                    #print("a: ",a," a1: ",a1)
                    #print("len",len(result[a])) 
                    #print("a:",a," a1: ",a1,"result: ",result[a][a1]) 
                    result[a][a1] = tokens[result[a][a1]] 
                except KeyError:
                    #print("na") 
                    pass 
        for a in range(0,len(result),1): 
            if "" in result[a]: 
                num = result[a].count("")
                #print("num: ",num)
                #print("a ",a) 
                while num > 0:
                    result[a].remove("")
                    num = num -1
        for a in result:
            if a == []:
                result.remove([]) 
        print(result) 
        return result     
        #print(resul) 

class parser:
    def __init__(self):
        self.ast = [] 
        self.variables = {}  
    def parse(self,inp):
        line_number = 0 
        while len(inp) > line_number:
            token = self.parse_line(inp[line_number],line_number,inp)
            print("token", token) 
            if token[0] == "whileprim":
                #print("whilepirm detected") 
                line_number = line_number + len(token[2]) +1 #skip body adn crparen
            else:
                line_number = line_number + 1  
            self.ast.append(token) 
            #line_number = line_number +1 
        print(self.ast) 
        return self.ast 
    def parse_line(self,inp,line_number,fulltext): 
            for a in range(0,len(inp),1): 
                if inp[a] == "int":
                   #self.ast.append(["int",inp[a+1]])
                   self.variables[inp[a+1]]=int 
                   return ["int",inp[a+1]] 
                elif inp[a] == "float":
                   #self.ast.append(["float",inp[a+1]]) 
                   self.variables[inp[a+1]]=float
                   return ["float",inp[a+1]] 
                elif inp[a] == "string":
                   #self.ast.append(["string",inp[a+1]])
                   self.variables[inp[a+1]]=str 
                   return ["string",inp[a+1]] 
                elif inp[a] == "bool":
                   #self.ast.append(["bool",inp[a+1]]) 
                   self.variables[inp[a+1]]=bool
                   return ["bool",inp[a+1]] 
                elif inp[a] == "set": 
                    #print("entered set") 
                    if inp[a-1] in list(self.variables.keys()):
                       #print(inp[a-1], " is in variables")   
                       to_parse = []
                       if a == len(inp)-2: #straught equality
                           print("straight equality") 
                           return ["set",inp[a-1],inp[a+1]] 
                       for a2 in range(a+1,len(inp),1): # the token should not be ayn element 
                           to_parse.append(inp[a2])
                       return ["set",inp[a-1],self.parse_line(to_parse,line_number,fulltext)]  
                    else:
                       print("Error on line: ",line_number) 
                       print("Error l ", line_number,"can not assign to object") 
                elif inp[a] == "add":
                    if a == len(inp)-2: #we are at the end of line
                        return ["add",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["add",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "sub":
                    if a == len(inp)-2: #we are at the end of line
                        return ["sub",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["sub",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "mul":
                    if a == len(inp)-2: #we are at the end of line
                        return ["mul",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["mul",inp[a-1],self.parse_line(to_parse,line_number,fulltext)]
                elif inp[a] == "div":
                    if a == len(inp)-2: #we are at the end of line
                        return ["div",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["div",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "break":
                    return ["break"]
                elif inp[a] == "exit":
                    return ["exit"] 
                elif inp[a] == "print":
                    return ["print",inp[a+2]]
                elif inp[a] == "INC":
                    return ["INC",inp[a+1]]
                elif inp[a] == "++":
                    return ["INC",inp[a-1]]  
                elif inp[a] == "DEC" or inp[a] == "--": 
                    return ["DEC",inp[a+1]]
                elif inp[a] =="--":
                    return ["DEC",inp[a-1]] 
                elif inp[a] == "getline":
                    return ["getline",inp[a+2]]
                elif inp[a] == "eq":
                    if a == len(inp)-2: #we are at the end of line
                        return ["eq",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["eq",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "neq":
                    if a == len(inp)-2: #we are at the end of line
                        return ["neq",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["neq",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "gr":
                    if a == len(inp)-2: #we are at the end of line
                        return ["gr",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["gr",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "sm":
                    if a == len(inp)-2: #we are at the end of line
                        return ["sm",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["sm",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "gre":
                    if a == len(inp)-2: #we are at the end of line
                        return ["gre",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["gre",inp[a-1],self.parse_line(to_parse,line_number,fulltext)] 
                elif inp[a] == "sme":
                    if a == len(inp)-2: #we are at the end of line
                        return ["sme",inp[a-1],inp[a+1]]
                    else:
                        to_parse = [] 
                        for a1 in range(a+1,len(inp),1):
                            to_parse.append(inp[a1])
                        return ["sme",inp[a-1],self.parse_line(to_parse,line_number,fulltext)]
                elif inp[a] == "whileprim": 
                    cond = [] 
                    body = []  
                    if inp[a+1] != "lparen":
                        print("Expected ( after while") 
                        exit(1) 
                    counter = a
                    try:
                        while inp[counter] != "rparen":
                            cond.append(inp[counter]) 
                            counter = counter +1 
                    except IndexError: 
                        print("Error: Missinmg (")
                        exit(1) 
                    par = parser()
                    clparen = 1
                    crparen = 0
                    counter = counter +2 #next token after rparen 
                    lno = line_number 
                    to_parse = [] 
                    tmp = [] 
                    try: 
                        while clparen != crparen:
                            #print("clparen:",clparen, " ", lno)
                            #print("token",counter)
                            #print("Current line: ",fulltext[lno]) 
                            if fulltext[lno][counter] == "clparen":
                                clparen = clparen +1 
                                tmp.append("clparen") 
                            elif fulltext[lno][counter] == "crparen":
                                crparen = crparen + 1
                                if crparen != clparen:
                                    tmp.append("crparen") #this is not the end 
                            else:
                                tmp.append(fulltext[lno][counter]) 
                                
                            if counter+1 == len(fulltext[lno]) and tmp!=[]: #end of line 
                                lno = lno +1 
                                counter = 0
                                to_parse.append(tmp) 
                                tmp = [] 
                            else:
                                counter = counter +1  #next token 
                    except IndexError:
                        print("Error: Expected } at end of while block")
                        exit(1)
                    #print("reached parser") 
                    print(to_parse)
                    body = par.parse(to_parse)
                    return ["whileprim",cond,body]
                            






#["whilePrim",cond,body] 
#cond : lparen stuff rparem
#body : clparen stuff rparen 
def main():
    global name
    global out 
    getname() 
    out = name 
    gettext() 
    print(text) 
    print()
    test = lexer()
    lexems = test.lex(text) 
    print() 
    par = parser()
    par.parse(lexems)

def getname():
    global name 
    #getÄ's the name of the file to process from the command line 
    if len(sys.argv) == 1:
        print("No input filem stopping")
    elif len(sys.argv) == 2:
        name = sys.argv[1] 
    else:
        print("Too many arguments") 

def gettext():
    #get the program
    global name
    global text  
    try: 
        with open(name,"r") as file: 
            text = file.read()
    except FileNotFoundError:
        print("File not found, stopping") 

if __name__ == "__main__":
    main() 
